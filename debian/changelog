eyrie-utilities (15) unstable; urgency=medium

  * renew-certificate: Pass the --non-interactive flag to certbot to
    suppress warning messages with new versions.
  * renew-certificate: Also refresh the copy of the TLS certificate used
    by PostgreSQL.
  * Update tests and test framework to (unreleased) rra-c-util 11.0.0.

 -- Russ Allbery <rra@debian.org>  Thu, 28 Mar 2024 19:55:49 -0700

eyrie-utilities (14) unstable; urgency=medium

  * renew-certificate: Fix the path to which the news.eyrie.org
    certificate private key is copied.
  * renew-certificate: Merge the inn.eyrie.org certificate with the
    hope.eyrie.org certificate, since they are both mostly redirect sites
    at this point.
  * Update tests and test framework to (unreleased) rra-c-util 11.0.0.
  * Fix warnings from some of the standard tests due to the lack of a lib
    or blib directory in this package.
  * Add dependency on libperl-critic-community-perl for increased
    perlcritic testing.
  * Update standards version to 4.6.2 (no changes required).

 -- Russ Allbery <rra@debian.org>  Sun, 31 Dec 2023 14:25:44 -0800

eyrie-utilities (13) unstable; urgency=medium

  * inn-purestart: New program to initialize an INN installation.
  * inn-unwanted: New program to analyze rejected news articles.
  * renew-certificate: Add support for news.eyrie.org, including copying
    the certificate to where nnrpd expects it.
  * Update tests and test framework to rra-c-util 8.4.
  * Update debhelper compatibility level to V13.

 -- Russ Allbery <rra@debian.org>  Sat, 22 Aug 2020 16:42:28 -0700

eyrie-utilities (12) unstable; urgency=medium

  * rebuild-iptables: Switch to using the default iptables-restore binary
    (which is now NFT-based).  Generate separate rules for IPv4 and IPv6
    since iptables-nft no longer supports ignoring --ipv4 or --ipv6 rules
    when not applicable.
  * Update tests and test framework to rra-c-util 8.2.
  * Update debhelper compatibility level to V12.
    - Use a debhelper-compat dependency instead of debian/compat.
  * Update standards version to 4.5.0.

 -- Russ Allbery <rra@debian.org>  Sat, 01 Feb 2020 15:19:01 -0800

eyrie-utilities (11) unstable; urgency=medium

  * rebuild-iptables: Use ip6tables-legacy-restore and
    iptables-legacy-restore by default if they are available, and stop
    using fully-qualified paths
  * Add dependency on iptables.
  * Update standards version to 4.2.1.
    - Make tests verbose.

 -- Russ Allbery <rra@debian.org>  Sat, 03 Nov 2018 14:56:36 -0700

eyrie-utilities (10) unstable; urgency=medium

  * git-create-public: Use an https URL for the cloneurl setting for
    newly-created repositories.
  * Skip tests if DEB_BUILD_OPTIONS includes nocheck.
  * Switch to an https URL for the debian/copyright format field.
  * Update standards version to 4.1.3 (no changes required).

 -- Russ Allbery <rra@debian.org>  Mon, 19 Feb 2018 18:16:03 -0800

eyrie-utilities (9) unstable; urgency=medium

  * sbuild-update-all: Add -d flag to configure a delay before doing the
    update, since I can't find a better way to get the systemd timer to
    wait until the network comes up after restore from hibernation.

 -- Russ Allbery <rra@debian.org>  Tue, 26 Dec 2017 17:53:32 -0800

eyrie-utilities (8) unstable; urgency=medium

  * sbuild-update-all: New script to update all sbuild source chroots and
    mail the results to an email address.
  * Set Rules-Requires-Root: no.
  * Update debhelper compatibility level to V11.
  * Update standards version to 4.1.2.
    - Set the package priority to optional, since extra has been retired.

 -- Russ Allbery <rra@debian.org>  Sun, 17 Dec 2017 17:25:58 -0800

eyrie-utilities (7) unstable; urgency=medium

  * Add __END_OF_DOCS__ tokens to properly close the heredocs in
    knock-backend and renew-certificate.
  * pkinit-ca: New script that automates OpenSSL operations to create a
    CA, KDC, and client certificates for Kerberos PKINIT.
  * Add stopwords to all of the POD documentation so that spelling tests
    pass cleanly.
  * Enable AUTHOR_TESTING during normal builds so that coding style and
    POD spelling tests will run.
  * Add aspell, aspell-en, and libtest-spelling-perl to the build
    dependencies.
  * Switch to /usr/share/dpkg/pkg-info.mk instead of calling
    dh_listpackages and dpkg-parsechangelog to gather package name and
    version information.
  * Update standards version to 4.0.0.

 -- Russ Allbery <rra@debian.org>  Sun, 02 Jul 2017 13:00:28 -0700

eyrie-utilities (6) unstable; urgency=medium

  * renew-certificate: New script that knows the right certbot flags to
    renew all eyrie.org certificates and does any necessary
    post-processing and service restarts.

 -- Russ Allbery <rra@debian.org>  Sat, 10 Jun 2017 16:58:48 -0700

eyrie-utilities (5) unstable; urgency=medium

  * knock-backend: New remctl backend script to allow a remote address to
    connect to ssh on a host, used to whitelist ssh from previously
    unknown addresses following a Kerberos authentication.

 -- Russ Allbery <rra@debian.org>  Fri, 02 Jun 2017 10:17:34 -0700

eyrie-utilities (4) unstable; urgency=medium

  * git-create-public: New script to create a public Git repository.
  * reprepro-upload: New tiny script to process a reprepro upload.

 -- Russ Allbery <rra@debian.org>  Thu, 01 Jun 2017 18:51:59 -0700

eyrie-utilities (3) unstable; urgency=medium

  * git-gc-all: New script to run git gc on every bare repository in a
    directory named *.git under a given path.
  * Add support for scripts in /usr/bin to the man page generation logic
    in debian/rules.
  * Use dpkg-parsechangelog -S to pull the Version field.

 -- Russ Allbery <rra@debian.org>  Thu, 01 Jun 2017 14:23:58 -0700

eyrie-utilities (2) unstable; urgency=medium

  * rebuild-iptables: Fix code that builds the list of iptables fragments
    to not use a closed directory handle as the name of the directory.

 -- Russ Allbery <rra@debian.org>  Sun, 15 Jan 2017 12:57:01 -0800

eyrie-utilities (1) unstable; urgency=medium

  * Initial package of various utilities for eyrie.org systems.

 -- Russ Allbery <rra@debian.org>  Sat, 14 Jan 2017 21:12:45 -0800
